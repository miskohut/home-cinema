import { TorrentState } from "./TorrentState";

const mongoose = require('mongoose')!;

const TorrentSchema = mongoose.Schema({
    magnet: String,
    name: String,
    downloaded: Number,
    uploaded: Number,
    downloadSpeed: Number,
    progress: Number,
    state: typeof TorrentState,
    fileSize: Number
});

export = mongoose.model('torrent', TorrentSchema);