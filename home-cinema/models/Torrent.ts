import { TorrentState } from "./TorrentState";

export class Torrent {
    private magnet: string;
    name!: string;
    downloaded!: number;
    uploaded!: number;
    downloadSpeed!: number;
    uploadSpeed!: number;
    progress!: number;
    state!: TorrentState;
    category!: string;
     

    constructor(magnet: string) {
        this.magnet = magnet;
    }

    getMagnet() {
        return this.magnet;
    }
}