import { Config } from "../config/Config";
import { Torrent } from "../models/Torrent";

const WebTorrent = require("webtorrent")!;
const TorrentModel = require("../models/TorrentModel");

export class TorrentService {
  private client: typeof WebTorrent;

  constructor() {}

  downloadWatchable() {}

  searchTorrent() {}

  downloadTorrent(torrent: typeof TorrentModel) {
    this.getClient().add(
      torrent.magnet,
      { path: Config.getInstance().getDefaultMoviePath() + torrent.category },
      (downloadedTorrent: any) => {
        //console.log(downloadedTorrent);
      }
    );
  }

  getActiveTorrents(torrents: typeof TorrentModel[]) {
    let torrentsResults: typeof TorrentModel[] = [];

    torrents.forEach((element) => {
      let torrent = this.getClient().get(element.magnet);
      if (torrent) {
        element.name = torrent.name;
        element.downloaded = torrent.downloaded;
        element.uploaded = torrent.uploaded;
        element.downloadSpeed = torrent.downloadSpeed;
        element.uploadSpeed = torrent.uploadSpeed;
        element.progress = torrent.progress * 100;
        element.fileSize = 0;

        torrent.files.forEach((file: any) => {
          element.fileSize += file.length;
        });

        torrentsResults.push(element);
      }
    }, this);

    return torrentsResults;
  }

  getTorrent(magnet: String) {
    let torrent = this.getClient().get(magnet);

    if (torrent) {
      let resultTorrent = new TorrentModel();
      resultTorrent.name = torrent.name;
      resultTorrent.downloaded = torrent.downloaded;
      resultTorrent.uploaded = torrent.uploaded;
      resultTorrent.downloadSpeed = torrent.downloadSpeed;
      resultTorrent.uploadSpeed = torrent.uploadSpeed;
      resultTorrent.progress = torrent.progress * 100;
      resultTorrent.fileSize = 0;

      torrent.files.forEach((file: any) => {
        resultTorrent.fileSize += file.length;
      });

      return resultTorrent;
    }
  }

  destroyTorrents() {
    this.getClient().destroy();
  }

  private getClient() {
    if (!this.client) {
      this.client = new WebTorrent();
    }
    return this.client;
  }

  removeTorrent(torrent: typeof TorrentModel) {
    this.getClient().remove(torrent.magnet);
  }

  pauseTorrent(torrent: typeof TorrentModel) {
    let clientTorrent = this.getClient().get(torrent.magnet);

    if (clientTorrent) {
      let returnTorrent = torrent;

      returnTorrent.name = clientTorrent.name;
      returnTorrent.downloaded = clientTorrent.downloaded;
      returnTorrent.uploaded = clientTorrent.uploaded;
      returnTorrent.downloadSpeed = clientTorrent.downloadSpeed;
      returnTorrent.uploadSpeed = clientTorrent.uploadSpeed;
      returnTorrent.progress = clientTorrent.progress * 100;
      returnTorrent.fileSize = clientTorrent.length;

      this.getClient().remove(returnTorrent.magnet);

      return returnTorrent;
    }
  }

  resumeTorrent(torrent: typeof TorrentModel) {
    this.getClient().add(
      torrent.magnet,
      { path: Config.getInstance().getDefaultMoviePath() },
      (downloadedTorrent: any) => {}
    );
  }
}
