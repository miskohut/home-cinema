const config = require("../config/config.json")!;

export class Config {
    private defaultPath: string;
    private static INSTANCE: Config;

    static getInstance() {
        if (!Config.INSTANCE) {
            Config.INSTANCE = new Config();
        }

        return Config.INSTANCE;
    }

    private constructor() {
        this.defaultPath = config.defaultMoviePath;
    }

    getDefaultMoviePath() {
        return this.defaultPath;
    }
}