import { TorrentState } from "../models/TorrentState";
import { TorrentService } from "../services/TorrentService";
const TorrentSearchApi = require("torrent-search-api");

var HTMLParser = require('node-html-parser');
const TorrentModel = require("../models/TorrentModel")!;

export class HomeCinema {
  torrentService = new TorrentService();
  private static INSTANCE: HomeCinema;
  private lastSearchResult: any = [];

  private constructor() {}

  static getInstance() {
    if (!HomeCinema.INSTANCE) {
      HomeCinema.INSTANCE = new HomeCinema();
    }

    return HomeCinema.INSTANCE;
  }

  async searchTorrent(searchString: String) {
    TorrentSearchApi.enableProvider("1337x");

    // Search '1080' in 'Movies' category and limit to 20 results
    this.lastSearchResult = await TorrentSearchApi.search(searchString);
    
   /* this.lastSearchResult.forEach( async (ele: any) => {
      console.log(await TorrentSearchApi.getTorrentDetails(ele).category);
    });*/

    return this.lastSearchResult;
  }

  async downloadTorrent(torrent: typeof TorrentModel) {
    let magnet = await TorrentSearchApi.getMagnet(this.lastSearchResult.find((element: any) => {return element.title === torrent.name}));
    let details = await TorrentSearchApi.getTorrentDetails(this.lastSearchResult.find((element: any) => {return element.title === torrent.name}));


    var root = HTMLParser.parse(details);
    let category = "movies";

    root.childNodes[3].childNodes[1].childNodes[3].childNodes.forEach((element: any) => {
      if (element.rawText === " Category TV ") {
        category = "TV";
        return;
      }
    });;

    torrent.magnet = magnet;
    torrent.category = category;
    this.torrentService.downloadTorrent(torrent);
    torrent.state = TorrentState.IN_PROGRESS;
    torrent.save((err: typeof Error, torrent: typeof TorrentModel) => {
    });
  }

  async getTorrents() {
    let torrents = await TorrentModel.find().exec();

    for (let i = 0; i < torrents.length; i++) {
      let activeTorrent = this.torrentService.getTorrent(torrents[i].magnet);
      if (activeTorrent) {
        torrents[i].name = activeTorrent.name;
        torrents[i].downloaded = activeTorrent.downloaded;
        torrents[i].uploaded = activeTorrent.uploaded;
        torrents[i].downloadSpeed = activeTorrent.downloadSpeed;
        torrents[i].uploadSpeed = activeTorrent.uploadSpeed;
        torrents[i].progress = activeTorrent.progress;
        torrents[i].state = TorrentState.IN_PROGRESS;
        torrents[i].fileSize = activeTorrent.fileSize;

        torrents[i].save();
      }

      activeTorrent = null;
    }

    return torrents;
  }

  removeTorrent(torrent: typeof TorrentModel) {
    this.torrentService.removeTorrent(torrent);
    torrent.remove();
  }

  async loadTorrents() {
    let torrents = (await TorrentModel.find().exec()) as typeof TorrentModel[];
    torrents.forEach((torrent) => {
      if (torrent.state === TorrentState.IN_PROGRESS) {
        this.torrentService.downloadTorrent(torrent);
      }
    });
  }

  async pauseTorrent(torrent: typeof TorrentModel) {
    torrent.state = TorrentState.PAUSED;
    this.torrentService.pauseTorrent(torrent);
    torrent.save();
  }

  async resumeTorrent(torrent: typeof TorrentModel) {
    torrent.state = TorrentState.IN_PROGRESS;
    torrent.save();
    this.torrentService.resumeTorrent(torrent);
  }
}
