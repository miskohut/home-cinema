import { ClientRequest, Server, ServerResponse } from "http";
import { HomeCinema } from '../../home-cinema/application/HomeCinema';

const express = require('express')!;
const router = express.Router();

const formidable = require('formidable');

const TorrentModel = require('../../home-cinema/models/TorrentModel')!;

router.get('/', async (request: ClientRequest, response: ServerResponse) => {
    response.write(JSON.stringify(await HomeCinema.getInstance().getTorrents()));
    response.end();
});

router.post('/download', (request: ClientRequest, response: ServerResponse) => {
    let body:any = [];
    request.on('data', (chunk) => {

        body.push(chunk);

      }).on('end', () => {

        body = JSON.parse(Buffer.concat(body).toString());

        if (body.title) {
          HomeCinema.getInstance().downloadTorrent(new TorrentModel({name: body.title}));
          response.end("Torrent download started");
        }

      });
});

router.post('/pause', async (request: ClientRequest, response: ServerResponse) => {
  console.log("pause");
  let body:any = [];
  request.on('data', (chunk) => {

      body.push(chunk);

    }).on('end', async () => {

      body = JSON.parse(Buffer.concat(body).toString());

      let torrent = await TorrentModel.findOne({ magnet: body.magnet }).exec();

      HomeCinema.getInstance().pauseTorrent(torrent);
      response.end("Torrent paused");

    });  
});

router.post('/resume', async (request: ClientRequest, response: ServerResponse) => {
  console.log("pause");
  let body:any = [];
  request.on('data', (chunk) => {

      body.push(chunk);

    }).on('end', async () => {

      body = JSON.parse(Buffer.concat(body).toString());

      let torrent = await TorrentModel.findOne({ magnet: body.magnet }).exec();

      HomeCinema.getInstance().resumeTorrent(torrent);
      response.end("Torrent resumed");

    });  
});

router.post('/remove', async (request: ClientRequest, response: ServerResponse) => {
  console.log("pause");
  let body:any = [];
  request.on('data', (chunk) => {

      body.push(chunk);

    }).on('end', async () => {

      body = JSON.parse(Buffer.concat(body).toString());

      let torrent = await TorrentModel.findOne({ magnet: body.magnet }).exec();

      HomeCinema.getInstance().removeTorrent(torrent);
      response.end("Torrent removed");

    });  
}); 

router.post('/search', async (request: ClientRequest, response: ServerResponse) => {
  console.log("pause");
  let body:any = [];
  request.on('data', (chunk) => {

      body.push(chunk);

    }).on('end', async () => {

      body = JSON.parse(Buffer.concat(body).toString());

      let torrents = await HomeCinema.getInstance().searchTorrent(body.search);
      response.write(JSON.stringify(torrents));
      response.end();

    });  
}); 

module.exports = router;
