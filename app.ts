import { HomeCinema } from "./home-cinema/application/HomeCinema";

const mongoose = require('mongoose')!;
mongoose.connect('mongodb://localhost:27017', {useNewUrlParser: true});

const express = require('express')!;
const app = express();

const cors = require('cors');


var corsOptions = {
    //origin: ['http://192.168.1.26:3000', 'http://192.168.1.88:3000' ],
    origin: true,
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
  };

const torrentRouter = require('./api/routers/TorrentRouter')!;

app.use(cors(corsOptions));
app.use('/', torrentRouter);

app.listen(3001, '192.168.0.155', () => {
    console.log('App started ...');
    HomeCinema.getInstance().loadTorrents();
});